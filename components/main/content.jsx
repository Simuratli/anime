import React,{useEffect,useState} from 'react'
import {Search,Card,Loader} from '../index.js'
import axios from 'axios'

function Content() {
    
    const [state, setState] = useState({
        data:null,
        loading:true
    })

    useEffect(() => {
        setState((prevData)=>({
            ...prevData,
            loader:true
        }))
        axios.get('https://api.jikan.moe/v3/top/anime/1')
        .then((res)=>{
           setState((prevData)=>({
               ...prevData,
               data:res.data.top.splice(0,5),
               loader:false
           }))
        })
    }, [])

    let CARD_DATA = state.data && state.data.map((anime,idx)=>{
        return <Card key={idx} data={anime}/>
    })

    if(state.loader) CARD_DATA = <Loader/>

    return (
        <div  className='manga'>
            <div className='search_container'>
                <Search/>
            </div>
            <h1 className='top_anime-head'>Top 5 anime</h1>
            <div className='top_5_anime'>
                {CARD_DATA}
            </div>
        </div>
    )
}

export default Content
