import React from 'react'

function Badge(props) {
    return (
        <span className='nbadge nbadge-green'>
            {props.text}
        </span>
    )
}

export default Badge
