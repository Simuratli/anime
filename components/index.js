import Navbar from './navbar/navbar'
import Sidebar from './sidebar/sidebar'
import Content from './main/content'
import Search from './Search/Search'
import Card from './card/card'
import Loader from './loader/loader'
import Badge from './badge/badge'
import Character from './CharacterCARD/Character'

export {Navbar,Sidebar,Content,Search,Card,Loader,Badge,Character}