import React,{useState} from 'react';
import {useRouter} from 'next/router'
import Link from 'next/link'

function Card(props) {
    const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
    ];
    let START_DATE, END_DATE;
    if (props.search) {
        END_DATE = `${monthNames[new Date(props.data.end_date).getMonth()].substring(0,3)} ${new Date(props.data.end_date).getFullYear()}`
        START_DATE = `${monthNames[new Date(props.data.start_date).getMonth()].substring(0,3)} ${new Date(props.data.start_date).getFullYear()}`
    }

    return (
        <div className='card'>
            <div className='card_image'>
               {!props.manga ? <Link href="/info/[id]" as={`/info/${props.data.mal_id}`}>
                <img src={props.data.image_url} />
                </Link> :  <Link href="/manga/[id]" as={`/manga/${props.data.mal_id}`}>
                <img src={props.data.image_url} />
                </Link>}
                
            </div>
            <div className='card_content'>
                <div className='card_ranking'>
                    <span className='badge'>Rank : {props.data.rank}</span>
                    <span className='badge'>Type : {props.data.type}</span>
                    {!props.manga && <span className='badge'>Score : {props.score}</span>}
                </div>
                {!props.manga ? <Link href="/info/[id]" as={`/info/${props.data.mal_id}`}>
                    <a className='card_name'>{props.data.title}</a>
                </Link> :
                <Link href="/manga/[id]" as={`/manga/${props.data.mal_id}`}>
                    <a className='card_name'>{props.data.title}</a>
                </Link>}
                {!props.manga && <div className='episodes'>Episodes : <span className='badge'>{props.data.episodes}</span></div> }
                
                <div className='card_date'>
                    <span className='badge'>{props.search ? START_DATE : props.data.start_date}</span>
                    <span className='badge'>{!props.data.end_date ? 'Continues' : props.data.end_date && props.search ? END_DATE : props.data.end_date}</span>
                </div>

            </div>
        </div>
    )
}




export default Card
