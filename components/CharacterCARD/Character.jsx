import React from 'react'
import { Badge } from "../index.js";
import Link from 'next/link'

function Character({data}) {
    return (
        
        <Link href="/character/[id]" as={`/character/${data.mal_id}`}>
            <div className='characterCard link'>
                <div className='characterCard_image'>
                    <img src={data.image_url} />
                </div>
                <div className='character_content'>
                    <h2>{data.name}</h2>
                    <div>
                        Role:<Badge text={data.role} />
                    </div>
                </div>
            </div>
        </Link>
         
    )
}

export default Character
