import React from 'react'
import Link from 'next/link'

function Navbar() {
    return (
        <div  className='navbar div1'>
            <Link href='/'>Anime</Link>
        </div>
    )
}

export default Navbar
