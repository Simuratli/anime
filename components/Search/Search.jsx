import React,{useState} from 'react'
import Image from 'next/image'
import { useRouter } from 'next/router'

function Search() {

    const router = useRouter()

    const [state, setState] = useState({
        search:''
    })

    function change(e){
        setState((prevValue)=>({
            ...prevValue,
            search:e.target.value
        }))
    }

    function search(e){
        e.preventDefault()
        router.push({
            pathname: '/anime/[name]',
            query: { name: state.search },
        })
    }

    return (
        <div  className='search'>
            <form onSubmit={search}>
                <input onChange={change} placeholder='Search for anime' />
                <button onClick={search}><Image width={20} height={20} src='/images/search.svg'/></button>
            </form>
        </div>
    )
}

export default Search
