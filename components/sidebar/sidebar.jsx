import React,{useState} from 'react'
import Link from 'next/link';
import Image from 'next/image'

function Sidebar() {
    
    
    const [toggle, setToggle] = useState(false);
    function toggleSidebar() {
        setToggle(!toggle)
        let backdrop = document.querySelector('.sidebar_backdrop');
    }
    


    return (
        <div onClick={()=>toggleSidebar()} className={`sidebar_backdrop ${toggle && 'active_backdrop'}`}>
            <div className={`sidebar ${toggle && 'active_sidebar'}`}>
            <Image onClick={()=>toggleSidebar()} className='toggle_nav_button' width={30} height={30} src='/images/arrow-left.svg' />
                <div className='nav-items'>
                    <Link href='/'>Home</Link>
                    <Link href='/top'>Top</Link>
                    <Link href='/anime'>Anime</Link>
                    <Link href='/manga'>Manga</Link>
                    <Link href='/characters'>Characters</Link>
                    <Link href='/about'>About us</Link>
                </div>
            </div>
        </div>
    )
}
 
export default Sidebar
