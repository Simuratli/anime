import React from 'react'
import { Card } from "../../components";
import axios from 'axios'
import dynamic from 'next/dynamic'
import Head from 'next/head'

function SearchAnime(props) {

    return (
        <div className='manga'>
        <Head>
        <meta
            key="description"
            name="description"
            content='Anime and Manga Searching application. Developed by Simuratli Eljan'
          />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            <title>Search</title>
            <link rel="icon" href="/favicon.ico" />
        </Head>
        <h3>Search Results</h3>
            <div className='manga_container'>
                {
                    props.data.map((item,idx)=>{
                        return <Card search={true} key={idx} data={item} />
                    })
                }
            </div>
        </div>
    )
}

SearchAnime.getInitialProps = async (ctx) => {
    const res = await axios.get(`https://api.jikan.moe/v3/search/anime?q=${ctx.query.name}/Zero&page=1`)
    const data = await res.data.results
    return { data: data,loading:false}
   
}

export default SearchAnime
