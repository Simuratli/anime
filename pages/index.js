import Head from 'next/head'
import {Navbar,Sidebar,Content} from '../components'

export default function Home() {
  return (
    <div>
      <Head>
      <meta
            key="description"
            name="description"
            content='Anime and Manga Searching application. Developed by Simuratli Eljan'
          />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <title>Anime</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
          <Content/>
      </main>
    </div>
  )
}
