import React,{useEffect,useState} from 'react'
import {Card,Loader} from '../components'
import axios from 'axios'
import Head from 'next/head'

function Manga() {
    
    const [state, setState] = useState({
        data:null,
        page:1,
        loader:true
    })

    function fetchManga() {
        setState(prev=>({
            ...prev,
            loader:true
        }))
        axios.get(`https://api.jikan.moe/v3/top/manga/${state.page}`)
        .then(res=>{
            setState(prev=>({
                ...prev,
                data:res.data.top,
                loader:false
            }))
        })
    }

    useEffect(() => {
        fetchManga()
    }, [state.page])

    function pageChange(change){
        if(change === 'prev'){
            setState(prev=>({
                ...prev,
                page:state.page-1,
            }))
            fetchManga()
        }else if(change === 'next'){
            setState(prev=>({
                ...prev,
                page:state.page+1,
            }))
            fetchManga()
        }
    }

    let MANGA_CARDS = state.data && state.data.map((manga,idx)=>{
        return <Card key={idx} manga={true} data={manga}/>
    })
    if(state.loader) MANGA_CARDS = <Loader/>

    return (
        <div className='manga'>
        <Head>
        <meta
            key="description"
            name="description"
            content='Anime and Manga Searching application. Developed by Simuratli Eljan'
          />
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            <title>Manga</title>
            <link rel="icon" href="/favicon.ico" />
        </Head>
            <h1 className='manga_title'>Manga</h1>
            <div className='manga_container'>
                {MANGA_CARDS}
            </div>
            <br/>
            <div className='next_page_container'>
                {state.page !== 1 &&  <button onClick={()=>{pageChange('prev')}} className='filter_button'>Prev</button>}
                <button onClick={()=>{pageChange('next')}} className='filter_button'>Next</button>
            </div>
            <br/>
        </div>
    )
}

export default Manga
