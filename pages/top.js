import React,{useEffect,useState} from 'react';
import {Card,Loader} from '../components'
import axios from 'axios';
import Head from 'next/head'

function Top() {

    const [state, setState] = useState({
        data:null,
        header:'Anime',
        loader:true,
        page:1,
        search:'anime'
    })

    function fetch(){
        setState((prev)=>({
            ...prev,
            loader:true
        }))
        axios.get(`https://api.jikan.moe/v3/top/${state.search}/${state.page}`)
        .then((res)=>{
            setState((prev)=>({
            ...prev,
            data:res.data.top,
            loader:false
        }))
        })
    }

    useEffect(() => {
        fetch()
    }, [state.search,state.page])

    function handleChange(category){
        if(category === 'anime'){
            // fetch()
            setState((prev)=>({
                ...prev,
                header:"Anime",
                search:'anime'
            }))
        }else if(category === 'manga'){
            // fetch()
            setState((prev)=>({
                ...prev,
                header:"Manga",
                search:'manga'
            }))
        }
    }

    function pageChange(num) {
        if(num === 'prev'){
            setState((prev)=>({
                ...prev,
                page:state.page - 1
            }))
        }else if(num === 'next'){
            setState((prev)=>({
                ...prev,
                page:state.page + 1
            }))
        }
    }

    let CARD_DATA = state.data && state.data.map((anime,idx)=>{
        return <Card manga={state.search === 'manga' && true} key={idx} data={anime} />
    })

    if(state.loader) CARD_DATA = <Loader/>

    return (
        <div className='manga'>
                <Head>
                <meta
            key="description"
            name="description"
            content='Anime and Manga Searching application. Developed by Simuratli Eljan'
          />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                    <title>Top Manga or anime</title>
                    <link rel="icon" href="/favicon.ico" />
                </Head>
                <div className='top_head_container'>
                    <h1 className='top_head'>Top: <button className='top_head-green'>{state.header}</button></h1>
                </div>
                <div className='filter_buttons_container'>
                    <div className='filter_buttons'>
                        <button onClick={()=>handleChange('anime')} className='filter_button'>Animes</button>
                        <button onClick={()=>handleChange('manga')} className='filter_button'>Mangas</button>
                    </div>
                </div>
                <div className='top_anime_cards'>
                    {CARD_DATA}
                </div>
                <br/>
                <div className='next_page_container'>
                    {state.page !== 1 &&  <button onClick={()=>{pageChange('prev')}} className='filter_button'>Prev</button>}
                    <button onClick={()=>{pageChange('next')}} className='filter_button'>Next</button>
                </div>
                <br/>
        </div>
    )
}

export default Top
