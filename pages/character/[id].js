import React from 'react'
import axios from 'axios'
import Head from 'next/head'

function CharacterPics(props) {

    let DATA = props.data && props.data.map((item,idx)=>{
        return <div className='pics' key={idx}>
            <img src={item.large}/>
        </div>
    })


    return (
        <div className='manga'>
        <Head>
        <meta
            key="description"
            name="description"
            content='Anime and Manga Searching application. Developed by Simuratli Eljan'
          />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            <title>Character Images</title>
            <link rel="icon" href="/favicon.ico" />
        </Head>
            <div className='characters'>
                {DATA}
            </div>    
        </div>
    )
}

CharacterPics.getInitialProps = async (ctx) => {
    const res = await axios.get(`https://api.jikan.moe/v3/character/${ctx.query.id}/pictures`)
    const data = await res.data.pictures
    return {data:data}
}

export default CharacterPics
