import React from 'react'
import Link from 'next/link'

function About() {
    return (
        <div className='manga about'>
            <h1>HI THERE!</h1> 
            <p>This app developed by <Link href='https://www.linkedin.com/in/elcan-simuratli-36678818a/'>Simuratli Eljan</Link> </p>
            <p>BitBucket :<Link href='https://bitbucket.org/Simuratli/'><img src='https://wac-cdn.atlassian.com/dam/jcr:bd56917e-e361-4f03-b672-9f5ef5b06e80/Bitbucket-blue.svg?cdnVersion=1462' /></Link> </p>
            <p>Github :<Link href='https://github.com/Simuratli'><img src="https://github.githubassets.com/images/modules/logos_page/GitHub-Logo.png" /></Link> </p>
        </div>
    )
}

export default About
