import React,{useEffect,useState} from 'react'
import {Card,Loader} from '../components'
import axios from 'axios'
import Head from 'next/head'

function Anime() {
    
    const [state, setState] = useState({
        data:null,
        page:1,
        loader:true
    })

    function fetchAnime() {
        setState(prev=>({
            ...prev,
            loader:true
        }))
        axios.get(`https://api.jikan.moe/v3/top/anime/${state.page}`)
        .then(res=>{
            setState(prev=>({
                ...prev,
                data:res.data.top,
                loader:false
            }))
        })
    }

    useEffect(() => {
        fetchAnime()
    }, [state.page])


    function pageChange(change){
        if(change === 'prev'){
            setState(prev=>({
                ...prev,
                page:state.page-1,
            }))
            fetchAnime()
        }else if(change === 'next'){
            setState(prev=>({
                ...prev,
                page:state.page+1,
            }))
            fetchAnime()
        }
    }

    
    let ANIME_CARDS = state.data && state.data.map((manga,idx)=>{
        return <Card key={idx}  data={manga}/>
    })
    if(state.loader) ANIME_CARDS = <Loader/>

    return (
        <div className='manga'> 
        <Head>
        <meta
            key="description"
            name="description"
            content='Anime and Manga Searching application. Developed by Simuratli Eljan'
          />
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            <title>Anime</title>
            <link rel="icon" href="/favicon.ico" />
        </Head>
            <h1 className='manga_title'>Anime</h1>
            <div className='manga_container'>
                {ANIME_CARDS}
            </div>
            <br/>
            <div className='next_page_container'>
                {state.page !== 1 &&  <button onClick={()=>{pageChange('prev')}} className='filter_button'>Prev</button>}
                <button onClick={()=>{pageChange('next')}} className='filter_button'>Next</button>
            </div>
            <br/>
        </div>
    )
}

export default Anime
