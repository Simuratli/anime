import React,{useEffect,useState} from 'react'
import {Character , Loader} from '../components'
import axios from "axios";
import Head from 'next/head'
function Characters() {
    const [state, setState] = useState({
        data:null,
        loading:true,
        page:1
    })

    async function fetchData(){
        setState((prev)=>({
            ...prev,
            loading:true
        }))
        const res = await axios.get(`https://api.jikan.moe/v3/manga/${state.page}/characters`)
        const data = await res.data.characters
        setState((prev)=>({
            ...prev,
            data:data,
            loading:false
        }))
    }

    useEffect(() => {
        fetchData()
    }, [state.page])


    function pageChange(change){
        if(change === 'prev'){
            setState(prev=>({
                ...prev,
                page:state.page-1,
            }))
            fetchData()
        }else if(change === 'next'){
            setState(prev=>({
                ...prev,
                page:state.page+1,
            }))
            fetchData()
        }
    }


    let CARD_DATA = state.data && state.data.map((item,idx)=>{
        return <Character key={idx} data={item} />
    })
    
    if(state.loading) CARD_DATA = <Loader/>

    return (
        <div className='manga'>
            <Head>
            <meta
            key="description"
            name="description"
            content='Anime and Manga Searching application. Developed by Simuratli Eljan'
            />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <title>Characters</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <h2>Characters</h2>
            <div className='characters'>
                {CARD_DATA}
            </div>   
            <br/>
            <div className='next_page_container'>
                {state.page !== 1 &&  <button onClick={()=>{pageChange('prev')}} className='filter_button'>Prev</button>}
                <button onClick={()=>{pageChange('next')}} className='filter_button'>Next</button>
            </div>
            <br/>
        </div>
    )
}

export default Characters
