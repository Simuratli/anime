import '../styles/globals.css'
import {Navbar,Sidebar} from '../components'
import Router from 'next/router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';

NProgress.configure({ ease: 'ease', speed: 500 }); // Adjust animation settings using easing (a CSS easing string) and speed (in ms). (default: ease and 200)
NProgress.configure({trickleSpeed: 800 }); //Adjust how often to trickle/increment, in ms.
NProgress.configure({ showSpinner: false });//Turn off loading spinner by setting it to false. (default: true)

Router.events.on('routeChangeStart', () => NProgress.start()); 
Router.events.on('routeChangeComplete', () => NProgress.done()); 
Router.events.on('routeChangeError', () => NProgress.done());

function MyApp({ Component, pageProps }) {
  return (<div>
      <style jsx>{`
          #nprogress .bar {
            height:15px;
          }
         
          #nprogress .bar {
            background: red;
          }

          #nprogress .spinner-icon {
            border-top-color: blue;
            border-left-color: blue;
          }
      `}</style>

      <Navbar/> 
      <Sidebar/> 
      <Component {...pageProps} />
  </div>)
}

export default MyApp
