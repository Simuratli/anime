import React,{useEffect, useprops} from 'react'
import {Router, useRouter} from 'next/router'
import {Badge, Loader} from '../../components'
import axios from 'axios'
import Head from 'next/head'


function MangaInfo(props) {
    


    let DATA = <>
                <div className='info_head'>
                    <h1>{props.data.title_english &&  props.data.title_english} ({props.data.title_japanese && <Badge text={props.data.title_japanese}/>})</h1>
                 </div>
                 <div className='info_main'>
                        <div className='info_image'>
                            <img src={props.data.image_url && props.data.image_url} />
                        </div>
                        <div className='info_content'>
                            <div><span className='title_span'>Type :</span>{props.data.type && <Badge text={props.data.type}/>}</div>
                            <div><span className='title_span'>source:</span>{props.data.source && <Badge text={props.data.source}/>}</div>
                            <div><span className='title_span'>EPOSIDES:</span>{props.data.episodes && <Badge text={props.data.episodes}/> } </div>
                            <div><span className='title_span'>STATUS:</span>{props.data.status && <Badge text={props.data.status}/> }</div>
                            <div><span className='title_span'>Aired:</span> {props.data.aired && <Badge text={props.data.aired.string}/> }</div>
                            <div><span className='title_span'>dURATION:</span>{props.data.duration && <Badge text={props.data.duration}/> }</div>
                            <div><span className='title_span'>RATING:</span> {props.data.rating && <Badge text={props.data.rating}/> }</div>
                            <div><span className='title_span'>rANK:</span> {props.data.rank && <Badge text={props.data.rank}/>} </div>
                            <div><span className='title_span'>pOPULARITY:</span> {props.data.popularity && <Badge text={props.data.popularity}/>}</div>
                            <div><span className='title_span'>pRODUCERS:</span> {props.data.producers && props.data.producers.map((item)=>{
                                return <Badge key={item.name} text={item.name}/>
                            })}</div>
                            <div><span className='title_span'>sTUDIOS:</span> <Badge text='Tv'/></div>
                            <div><span className='title_span'>gENRES:</span> {props.data.genres && props.data.genres.map((item)=>{
                                return <Badge key={item.name} text={item.name}/>
                            })} </div>
                            <div><span className='title_span'>oPENING THEMES:</span> {props.data.opening_themes && props.data.opening_themes.map((item)=>{
                                return <Badge key={item} text={item}/>
                            })}</div>
                            <div><span className='title_span'>eNDING THEMES:</span> {props.data.ending_themes && props.data.ending_themes.map((item)=>{
                                return <Badge key={item} text={item}/>
                            })}</div>
                            <div><span className='title_span'>sYNOPSIS: </span>{props.data.synopsis && props.data.synopsis}</div>
                            
                        </div>
                </div>
    </> 

    if(props.data.length === 0) DATA = <Loader/>

    return (
        <div className='manga'>
        <Head>
        <meta
            key="description"
            name="description"
            content='Anime and Manga Searching application. Developed by Simuratli Eljan'
          />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            <title>{props.data.title && props.data.title}</title>
            <link rel="icon" href="/favicon.ico" />
        </Head>
             <div className='info'>
                 {DATA}
             </div>
        </div>
    )
}

MangaInfo.getInitialProps = async (ctx) => {
    const res = await axios.get(`https://api.jikan.moe/v3/manga/${ctx.query.id}`)
    const data = await res.data
    return { data: data,loading:false}
  }
  

export default MangaInfo
